<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%--<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">--%>
    <title>Add task to project</title>
</head>
<body>
<%@include file="common/menu.jspf" %>
<form:form id="addTaskToProjectForm" modelAttribute="taskToProjectAddingForm" action="addTaskToProjectProcess"
           method="post">
    <table>
        <form:hidden path="projectId" value="${projectId}"/>
        <tr>
            <td><form:label path="name">Task name:</form:label></td>
            <td><form:input path="name" name="projectName" id="projectName"/>
            </td>
        </tr>

        <tr>
            <td><form:label path="description">Description:</form:label></td>
            <td><form:input path="description" name="description" id="description"/>
            </td>
        </tr>

        <tr>
            <td>User:</td>
            <td>
                <form:select path="userId">
                    <form:option value="${null}" label=""/>
                    <form:options items="${users}" itemValue="id"/>
                </form:select>
            </td>
        </tr>

        <tr>
            <td><form:label path="predictedTimeForTask">Predicted time for task:</form:label></td>
            <td><form:input path="predictedTimeForTask" name="predictedTimeForTask" id="predictedTimeForTask"/>
            </td>
        </tr>

        <tr>
            <td><form:label path="startDate">Start date:</form:label></td>
            <fmt:formatDate value="${taskToProjectAddingForm.startDate}" var="startDateString" pattern="dd/MM/yyyy"/>
            <td><form:input path="startDate" value="${startDateString}" /></td>
        </tr>

        <tr>
            <td><form:label path="endDate">End date:</form:label></td>
            <fmt:formatDate value="${taskToProjectAddingForm.endDate}" var="endDateString" pattern="dd/MM/yyyy"/>
            <td><form:input path="endDate" value="${endDateString}" /></td>
        </tr>

        <tr>
            <td></td>
            <td><form:button id="addTaskToProject" name="addTaskToProject">Add task to project</form:button>
            </td>
        </tr>
        <tr></tr>

    </table>
    <table>
        <tr>
            <td style="font-style: italic; color: red;">${message}</td>
        </tr>
    </table>
</form:form>


</body>
</html>