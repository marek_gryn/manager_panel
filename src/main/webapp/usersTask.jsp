<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Your tasks</title>
</head>
<body>
<%@include file="common/menu.jspf" %>
<fieldset>
    <legend><h3>Your tasks</h3></legend>

    <c:choose>
        <c:when test="${currentUser.taskList.size()==0 || currentUser.taskList==null}">
            <b>Actually you don't have tasks</b>
        </c:when>
        <c:otherwise>
            <br>
            <table border="1" cellpadding="5">
                <tr>
                    <th>Project</th>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Predicted time for task [h]</th>
                    <th>Current spent time [h]</th>
                    <th>Percent of completion</th>
                    <th>Start date</th>
                    <th>End date</th>
                    <th>Finished</th>
                    <th>Action</th>
                </tr>
                <c:forEach items="${currentUser.taskList}" var="task">
                    <td><c:out value="${task.project.name}"/></td>
                    <td><c:out value="${task.name}"/></td>
                    <td><c:out value="${task.description}"/></td>
                    <td><c:out value="${task.predictedTimeForTask}"/></td>
                    <td><c:out value="${task.currentSpentTime}"/></td>
                    <td><c:out value="${task.percetOfCompletion}"/> %</td>
                    <fmt:formatDate value="${task.startDate}" var="startDateString" pattern="dd/MM/yyyy"/>
                    <td><c:out value="${startDateString}"/></td>
                    <fmt:formatDate value="${task.endDate}" var="endDateString" pattern="dd/MM/yyyy"/>
                    <td><c:out value="${endDateString}"/></td>
                    <c:choose>
                        <c:when test="${task.isFinished}">
                            <td>YES</td>
                        </c:when>
                        <c:otherwise>
                            <td>NO</td>
                        </c:otherwise>
                    </c:choose>
                    <td>
                        <form name="updateTaskButton" action="updateTaskForm" method="post">
                            <input type="hidden" name="taskIdToUpdate" value="${task.id}"/>
                            <input type="submit" name="submit" value="Update task"/>
                        </form>
                    </td>
                    </tr>
                </c:forEach>
            </table>
        </c:otherwise>
    </c:choose>
</fieldset>
</body>
</html>