<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%--<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">--%>
    <title>Edit user</title>
</head>
<body>
<%@include file="common/menu.jspf" %>

<form:form id="editForm" modelAttribute="userToEdit" action="editUserProcess"
           method="post">
    <table>
        <tr>
            <td><form:label path="id">ID:</form:label></td>
            <td><form:input path="id" name="id" id="id" readonly="true" />
            </td>
        </tr>

        <tr>
            <td><form:label path="userName">User name:</form:label></td>
            <td><form:input path="userName" name="userName" id="userName" readonly="true" />
            </td>
        </tr>

        <tr>
            <td><form:label path="name">Firstname:</form:label></td>
            <td><form:input path="name" name="name" id="name"/>
            </td>
        </tr>

        <tr>
            <td><form:label path="surname">Surname:</form:label></td>
            <td><form:input path="surname" name="surname" id="surname"/>
            </td>
        </tr>

        <tr>
            <td><form:label path="mail">Mail:</form:label></td>
            <td><form:input path="mail" name="surname" id="surname"/>
            </td>
        </tr>

        <tr>
            <td><form:label path="password">Password:</form:label></td>
            <td><form:password path="password" name="password"
                               id="password"/></td>
        </tr>

        <tr>
            <td>Program role:</td>
            <td>
                <form:select path="programRole">
                    <form:options items="${programRoles}"/>
                </form:select>
            </td>
        </tr>

        <tr>
            <td></td>
            <td><form:button id="edit" name="edit">Edit</form:button>
            </td>
        </tr>
        <tr></tr>

    </table>
    <table>
        <tr>
            <td style="font-style: italic; color: red;">${message}</td>
        </tr>
    </table>
</form:form>


</body>
</html>