<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<head>
    <%--<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">--%>
    <title>Adding user to project form</title>
</head>
<body>
<%@include file="common/menu.jspf" %>

<h3>Add user to project:</h3>

<form:form id="userToProjectAddingForm" modelAttribute="userToProjectAddingForm" action="addUserToProjectProcess"
           method="post">
    <table>

        <form:hidden path="projectId" value="${projectId}"/>
        <tr>
            <td>User:</td>
            <td>
                <form:select path="userId">
                    <form:options items="${users}" itemValue="id"/>
                </form:select>
            </td>
        </tr>

        <tr>
            <td>Project role:</td>
            <td>
                <form:select path="projectRole">
                    <form:options items="${projectRoles}"/>
                </form:select>
            </td>
        </tr>

        <tr>
            <td></td>
            <td><form:button id="addToProject" name="addToProject">Add to project</form:button>
            </td>
        </tr>
        <tr></tr>

    </table>
    <%--<table>--%>
        <%--<tr>--%>
            <%--<td style="font-style: italic; color: red;">${message}</td>--%>
        <%--</tr>--%>
    <%--</table>--%>
</form:form>


</body>
</html>