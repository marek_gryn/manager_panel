<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%--<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">--%>
    <title>Update task ${taskToUpdate.name}</title>
</head>
<body>
<%@include file="common/menu.jspf" %>

<form:form id="editForm" modelAttribute="taskToProjectEditForm" action="editTaskProcess"
           method="post">
    <table>

        <form:hidden path="taskIdToEdit" value="${taskToEdit.id}"/>
        <tr>
            <td><form:label path="name">Name:</form:label></td>
            <td><form:input path="name" name="name" id="name" value="${taskToEdit.name}"/>
            </td>
        </tr>

        <tr>
            <td><form:label path="description">Description:</form:label></td>
            <td><form:input path="description" name="description" id="description" value="${taskToEdit.description}"/>
            </td>
        </tr>

        <tr>
            <td><form:label path="predictedTimeForTask">Predicted Time For Task:</form:label></td>
            <td><form:input path="predictedTimeForTask" name="predictedTimeForTask" id="predictedTimeForTask" value="${taskToEdit.predictedTimeForTask}"/>
            </td>
        </tr>

        <tr>
            <td><form:label path="currentSpentTime">Current Spent Time:</form:label></td>
            <td><form:input path="currentSpentTime" name="currentSpentTime" id="currentSpentTime" value="${taskToEdit.currentSpentTime}"/></td>
        </tr>

        <tr>
            <td><form:label path="percetOfCompletion">Percent of Completion:</form:label></td>
            <td>
                <form:select path="percetOfCompletion">
                    <option value="${taskToEdit.percetOfCompletion}">${taskToEdit.percetOfCompletion}</option>
                    <option value="${0}">0</option>
                    <option value="${10}">10</option>
                    <option value="${20}">20</option>
                    <option value="${30}">30</option>
                    <option value="${40}">40</option>
                    <option value="${50}">50</option>
                    <option value="${60}">60</option>
                    <option value="${70}">70</option>
                    <option value="${80}">80</option>
                    <option value="${90}">90</option>
                    <option value="${100}">100</option>
                </form:select>
            </td>
        </tr>

        <tr>
            <td><form:label path="startDate">Start date:</form:label></td>
            <fmt:formatDate value="${taskToEdit.startDate}" var="startDateString" pattern="dd/MM/yyyy"/>
            <td><form:input path="startDate" value="${startDateString}" /></td>
        </tr>

        <tr>
            <td><form:label path="endDate">Start date:</form:label></td>
            <fmt:formatDate value="${taskToEdit.endDate}" var="endDateString" pattern="dd/MM/yyyy"/>
            <td><form:input path="endDate" value="${endDateString}" /></td>
        </tr>

        <tr>
            <td><form:label path="isFinished">Is finished?:</form:label></td>
            <td>
                <form:select path="isFinished">
                    <option value="${taskToEdit.isFinished}">${taskToEdit.isFinished}</option>
                    <option value="false">No</option>
                    <option value="true">Yes</option>
                </form:select>
            </td>
        </tr>
        <tr>
            <td>User:</td>
            <td>
                <form:select path="userId">
                    <form:option value="${taskToEdit.user.id}" label="Current user"/>
                    <form:options items="${users}" itemValue="id"/>
                </form:select>
            </td>
        </tr>
        <tr>
            <td></td>
            <td><form:button id="Edit" name="Edit">Edit task</form:button>
            </td>
        </tr>
        <tr></tr>

    </table>
    <table>
        <tr>
            <td style="font-style: italic; color: red;">${message}</td>
        </tr>
    </table>
</form:form>


</body>
</html>