<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%--<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">--%>
    <title>Registration</title>
</head>
<body>
<fieldset>
    <h3>Welcome in ManagerPanel</h3>
</fieldset>
<b>Please sign in:</b> </br>
<form:form id="logForm" modelAttribute="login" action="loginProcess"
           method="post">
    <table>
        <tr>
            <td><form:label path="userName">User name:</form:label></td>
            <td><form:input path="userName" name="userName" id="userName"/>
            </td>
        </tr>

        <tr>
            <td><form:label path="password">Password:</form:label></td>
            <td><form:password path="password" name="password" id="password"/></td>
        </tr>

        <tr>
            <td></td>
            <td><form:button id="login" name="login">Login</form:button>
            </td>
        </tr>
        <tr></tr>

    </table>
    <table>
        <tr>
            <td style="font-style: italic; color: red;">${message}</td>
        </tr>
    </table>
</form:form>

</body>
</html>