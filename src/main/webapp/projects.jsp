<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%--<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">--%>
    <title>Projects</title>
</head>
<body>
<%@include file="common/menu.jspf" %>
<c:if test="${programRole.equals('ADMIN') || programRole.equals('MANAGER')}">
<form name="addNewProject" action="addProject" method="get">
    <input type="submit" name="submit" value="Add new project" />
</form>
</c:if>

<table>
    <tr>
        <td style="font-style: italic; color: green;">${message}</td>
    </tr>
</table>

<c:choose>
    <c:when test="${empty projects}">
        <b>There is no projects yet</b>
    </c:when>
    <c:otherwise>
        <table border="1" cellpadding="10">
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Description</th>
                <th>Location</th>
                <th>Start date</th>
                <th>End date</th>
                <th>Action</th>
            </tr>
            <c:forEach items="${projects}" var="project">
                <tr>
                    <td><c:out value="${project.id}"></c:out></td>
                    <td><c:out value="${project.name}"></c:out></td>
                    <td><c:out value="${project.description}"></c:out></td>
                    <td><c:out value="${project.location}"></c:out></td>
                    <fmt:formatDate value="${project.startDate}" var="startDateString" pattern="dd/MM/yyyy" />
                    <td><c:out value="${startDateString}"></c:out></td>
                    <fmt:formatDate value="${project.endDate}" var="endDateString" pattern="dd/MM/yyyy" />
                    <td><c:out value="${endDateString}"></c:out></td>
                    <td>
                        <form name="details" action="projectDetails" method="post">
                            <input type="hidden" name="projectId" value="${project.id}"/>

                            <input type="submit" name="submit" value="Details" />
                        </form>

                        <%--<form name="editButton" action="editUser" method="post">--%>
                            <%--<input type="hidden" name="userIdToEdit" value="${user.id}"/>--%>
                            <%--<input type="submit" name="submit" value="Edit" />--%>
                        <%--</form>--%>

                    </td>
                </tr>
            </c:forEach>

        </table>
    </c:otherwise>
</c:choose>

</body>
</html>