<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%--<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">--%>
    <title>Registered users</title>
</head>
<br>
<%@include file="common/menu.jspf" %>
<table>
    <tr>
        <td style="font-style: italic; color: green;">${message}</td>
    </tr>
</table>
<b>Registered users</b> </br>

<c:if test="${programRole.equals('ADMIN')}">
<br>
<a href="register">Register new user</a>
<br>
</c:if>
<table border="1" cellpadding="10">
    <tr>
        <th>ID</th>
        <th>Username</th>
        <th>Name</th>
        <th>Surname</th>
        <th>Mail</th>
        <th>Program role</th>
        <th>Action</th>
    </tr>
    <c:forEach items="${users}" var="user">
        <tr>
            <td><c:out value="${user.id}"></c:out></td>
            <td><c:out value="${user.userName}"></c:out></td>
            <td><c:out value="${user.name}"></c:out></td>
            <td><c:out value="${user.surname}"></c:out></td>
            <td><c:out value="${user.mail}"></c:out></td>
            <td><c:out value="${user.programRole.toString()}"></c:out></td>
            <td>
            <div>

                <c:if test="${programRole.equals('ADMIN')}">
                    <form name="deleteButton" action="deleteUser" method="post">
                        <input type="hidden" name="userIdToDelete" value="${user.id}"/>
                        <input type="submit" name="submit" value="Delete"/>
                    </form>
                </c:if>
                <c:if test="${programRole.equals('ADMIN')}">
                <form name="editButton" action="editUser" method="post">
                    <input type="hidden" name="userIdToEdit" value="${user.id}"/>
                    <input type="submit" name="submit" value="Edit"/>
                </form>
                </c:if>
            </div>

            </td>
        </tr>
    </c:forEach>

</table>

</body>
</html>