<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<html>
<head>
    <title>Projects ${project.name} details</title>
</head>
<body>
<%@include file="common/menu.jspf" %>

<h3>Project details</h3>

<table>
    <tr>
        <td>
            <b>Project Id:</b>
        </td>
        <td>
            ${project.id}
        </td>
    </tr>
    <tr>
        <td>
            <b>Project name:</b>
        </td>
        <td>
            ${project.name}
        </td>
    </tr>
    <tr>
        <td>
            <b>Project description:</b>
        </td>
        <td>
            ${project.description}
        </td>
    </tr>
    <tr>
        <td>
            <b>Project location:</b>
        </td>
        <td>
            ${project.location}
        </td>
    </tr>
    <tr>
        <td>
            <b>Project start date:</b>
        </td>
        <td>
            <fmt:formatDate value="${project.startDate}" var="startDateString" pattern="dd/MM/yyyy" />
            ${startDateString}
        </td>
    </tr>
    <tr>
        <td>
            <b>Project end date:</b>
        </td>
        <td>
            <fmt:formatDate value="${project.endDate}" var="endDateString" pattern="dd/MM/yyyy" />
            ${endDateString}
        </td>
    </tr>
</table>


<table>
    <tr>
        <td style="font-style: italic; color: green;">${message}</td>
    </tr>
</table>
<table>
    <td valign="top">
        <%@include file="common/usersInProject.jspf" %></td>

    <td valign="top">
        <%@include file="common/tasksInProject.jspf" %>
    </td>
</table>



</body>
</html>