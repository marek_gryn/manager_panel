<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%--<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">--%>
    <title>Update task ${taskToUpdate.name}</title>
</head>
<body>
<%@include file="common/menu.jspf" %>

<form:form id="editForm" modelAttribute="taskToUpdate" action="updateTaskProcess"
           method="post">
    <table>

        <form:hidden path="id" value="${taskToUpdate.id}"/>
        <tr>
            <td><form:label path="name">Name:</form:label></td>
            <td><form:input path="name" name="name" id="name" readonly="true" />
            </td>
        </tr>

        <tr>
            <td><form:label path="description">Description:</form:label></td>
            <td><form:input path="description" name="description" id="description" readonly="true"/>
            </td>
        </tr>

        <tr>
            <td><form:label path="predictedTimeForTask">Predicted Time For Task:</form:label></td>
            <td><form:input path="predictedTimeForTask" name="predictedTimeForTask" id="predictedTimeForTask" readonly="true"/>
            </td>
        </tr>

        <tr>
            <td><form:label path="currentSpentTime">Current Spent Time:</form:label></td>
            <td><form:input path="currentSpentTime" name="currentSpentTime" id="currentSpentTime" /></td>
        </tr>

        <tr>
            <td><form:label path="percetOfCompletion">Percent of Completion:</form:label></td>
            <td>
                <form:select path="percetOfCompletion">
                    <option value="${taskToUpdate.percetOfCompletion}">${taskToUpdate.percetOfCompletion}</option>
                    <option value="${0}">0</option>
                    <option value="${10}">10</option>
                    <option value="${20}">20</option>
                    <option value="${30}">30</option>
                    <option value="${40}">40</option>
                    <option value="${50}">50</option>
                    <option value="${60}">60</option>
                    <option value="${70}">70</option>
                    <option value="${80}">80</option>
                    <option value="${90}">90</option>
                    <option value="${100}">100</option>
                </form:select>
            </td>
        </tr>

        <tr>
            <td><form:label path="startDate">Start date:</form:label></td>
            <fmt:formatDate value="${taskToUpdate.startDate}" var="startDateString" pattern="dd/MM/yyyy"/>
            <td><form:input path="startDate" value="${startDateString}" readonly="true"/></td>
        </tr>

        <tr>
            <td><form:label path="endDate">Start date:</form:label></td>
            <fmt:formatDate value="${taskToUpdate.endDate}" var="endDateString" pattern="dd/MM/yyyy"/>
            <td><form:input path="endDate" value="${endDateString}" readonly="true"/></td>
        </tr>

        <tr>
            <td><form:label path="isFinished">Is finished?:</form:label></td>
            <td>
                <form:select path="isFinished">
                    <option value="false">No</option>
                    <option value="true">Yes</option>
                </form:select>
            </td>
        </tr>

        <tr>
            <td></td>
            <td><form:button id="Update" name="Update">Update task</form:button>
            </td>
        </tr>
        <tr></tr>

    </table>
    <table>
        <tr>
            <td style="font-style: italic; color: red;">${message}</td>
        </tr>
    </table>
</form:form>


</body>
</html>