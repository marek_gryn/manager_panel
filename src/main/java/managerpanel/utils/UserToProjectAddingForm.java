package managerpanel.utils;

import managerpanel.enums.ProjectRole;
import managerpanel.model.User;

public class UserToProjectAddingForm {
    private Long projectId;
    private Long userId;
    private ProjectRole projectRole;

    public UserToProjectAddingForm() {
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public ProjectRole getProjectRole() {
        return projectRole;
    }

    public void setProjectRole(ProjectRole projectRole) {
        this.projectRole = projectRole;
    }
}
