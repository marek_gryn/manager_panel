package managerpanel.utils;

import managerpanel.enums.ProjectRole;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class TaskToProjectAddingForm {
    private Long taskIdToEdit;
    private Long projectId;
    private String name;
    private String description;
    private Integer predictedTimeForTask;
    private Long userId;
    private Long percetOfCompletion;
    private Integer currentSpentTime;
    private Boolean isFinished;

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date startDate;

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date endDate;

    public TaskToProjectAddingForm() {
    }

    public Long getTaskIdToEdit() {
        return taskIdToEdit;
    }

    public void setTaskIdToEdit(Long taskIdToEdit) {
        this.taskIdToEdit = taskIdToEdit;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getPredictedTimeForTask() {
        return predictedTimeForTask;
    }

    public void setPredictedTimeForTask(Integer predictedTimeForTask) {
        this.predictedTimeForTask = predictedTimeForTask;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getPercetOfCompletion() {
        return percetOfCompletion;
    }

    public void setPercetOfCompletion(Long percetOfCompletion) {
        this.percetOfCompletion = percetOfCompletion;
    }

    public Integer getCurrentSpentTime() {
        return currentSpentTime;
    }

    public void setCurrentSpentTime(Integer currentSpentTime) {
        this.currentSpentTime = currentSpentTime;
    }

    public Boolean getIsFinished() {
        return isFinished;
    }

    public void setIsFinished(Boolean finished) {
        isFinished = finished;
    }
}
