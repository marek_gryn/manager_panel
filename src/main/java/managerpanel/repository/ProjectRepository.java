package managerpanel.repository;

import managerpanel.enums.ProjectRole;
import managerpanel.model.Project;
import managerpanel.model.User;
import org.hibernate.annotations.SQLUpdate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Long> {

    Project findByName(String name);



    @Transactional
    @Modifying
    @Query(value = "DELETE FROM users_projectrole WHERE usersInProject_KEY= :userId AND Project_id= :projectId", nativeQuery = true )
    void romoveUserFromProject(@Param("userId") Long userId, @Param("projectId") Long projectId);
}

