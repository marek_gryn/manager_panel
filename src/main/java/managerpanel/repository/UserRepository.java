package managerpanel.repository;

import managerpanel.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Long>{
    User findByUserName(String userName);

    User findById(Long id);
    List<User> findAllBy();
}
