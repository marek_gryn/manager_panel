package managerpanel.service;

import managerpanel.enums.ProjectRole;
import managerpanel.model.Project;
import managerpanel.model.Task;
import managerpanel.model.User;
import managerpanel.repository.ProjectRepository;
import managerpanel.repository.TaskRepository;
import managerpanel.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
public class ProjectService {

    @Autowired
    ProjectRepository projectRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    TaskRepository taskRepository;

    @Transactional
    public boolean addProject(Project project) {

        if (project == null) {
            return false;
        }

        Project projectFromDataBase = projectRepository.findByName(project.getName());
        if (projectFromDataBase != null) {
            return false;
        }

        if (project.getName().equals("")
                || project.getLocation().equals("")
                || project.getDescription().equals("")
                || project.getStartDate()==null
                || project.getEndDate()==null) {
            return false;
        }

        if (project.getEndDate().before(project.getStartDate())) {
            return false;
        }

        projectRepository.save(project);

        return true;
    }

    public List<Project> getAllProjects() {
        List<Project> projects = projectRepository.findAll();

        return projects;
    }

    public Project getProject(Long id) {
        return projectRepository.findOne(id);
    }

    @Transactional
    public boolean addUserToProject(Long projectId, Long userId, ProjectRole projectRole) {
        Project project = projectRepository.findOne(projectId);
        User user = userRepository.findOne(userId);

        project.getUsersInProject().put(user, projectRole);

        projectRepository.save(project);
        userRepository.save(user);
        return true;
    }

    @Transactional
    public void removeUserFromProject(User user, Project project) {
        Set<Task> tasksInProject=project.getTaskList();

        for (Task t : tasksInProject) {
            if (t.getUser() !=null) {
                if (t.getUser().getId() == user.getId()) {
                    t.setUser(null);
                    taskRepository.save(t);
                }
            }
        }


        List<Task> userTasks=user.getTaskList();
        List<Task> tasksToRemove = new ArrayList<>();

        for (Task t : userTasks) {
            if (t.getProject().getId() == project.getId()) {
                tasksToRemove.add(t);
            }
        }
        if (tasksToRemove.size()!=0) {
            userTasks.removeAll(tasksToRemove);
        }



        project.getUsersInProject().remove(user);
        projectRepository.save(project);
        userRepository.save(user);
        projectRepository.romoveUserFromProject(user.getId(),project.getId());
    }

}
