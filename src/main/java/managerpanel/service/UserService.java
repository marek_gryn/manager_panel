package managerpanel.service;

import managerpanel.model.Login;
import managerpanel.model.User;
import managerpanel.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.Cookie;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;


@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    private static Map<String, User> currentlyLoggedUsers;


    public UserService() {
        this.currentlyLoggedUsers = new HashMap<>();
        System.out.println(hashCode());
    }

    public Map<String, User> getCurrentlyLoggedUsers() {
        return currentlyLoggedUsers;
    }

    public void setCurrentlyLoggedUsers(Map<String, User> currentlyLoggedUsers) {
        this.currentlyLoggedUsers = currentlyLoggedUsers;
    }

    @Transactional
    public boolean register(User user) {
        if (user.getUserName().equals("")
                || user.getName().equals("")
                || user.getSurname().equals("")
                || user.getMail().equals("")
                || user.getPassword().equals("")) {
            return false;
        }

        User userFromDataBase = userRepository.findByUserName(user.getUserName());
        if (userFromDataBase == null) {
            userRepository.save(user);
            return true;
        }
        return false;
    }


    public boolean login(Login login, String cookieValue) {
        if (login.getUserName().equals("")
                || login.getPassword().equals("")) {
            return false;
        }
        User userFromDataBase = userRepository.findByUserName(login.getUserName());
        if (userFromDataBase == null) {
            return false;
        }
        if (userFromDataBase.getPassword().equals(login.getPassword())) {
            addUserToCurrentlyLoggedUsersList(userFromDataBase, cookieValue);
            return true;
        }
        return false;
    }

    private void addUserToCurrentlyLoggedUsersList(User user, String cookieValue) {
        if (currentlyLoggedUsers == null) {
            System.out.println("mapa była nullem :O");
            currentlyLoggedUsers=new HashMap<>();
        }

        currentlyLoggedUsers.put(cookieValue, user);
    }

//    @Transactional
    public Optional<User> returnOptionalUserIfLogged(Cookie[] cookies) {

        if (cookies == null) {
            return Optional.empty();
        }

        for (Cookie c : cookies) {
            if (c.getName().equals("loginControl")) {
                if (currentlyLoggedUsers.containsKey(c.getValue())) {
                    User user=userRepository.findById(currentlyLoggedUsers.get(c.getValue()).getId());
                    return Optional.of(user);
                }
            }
        }
        return Optional.empty();
    }

    public void logout(String cookieValue) {
        currentlyLoggedUsers.remove(cookieValue);
    }


    public List<User> findAllUsers() {
        return userRepository.findAll();
    }

    public User findUser(Long id) {
        User user=userRepository.findOne(id);
        return user;
    }

    @Transactional
    public void deleteUser(Long id) {
        userRepository.delete(id);
    }

    @Transactional
    public boolean editUser(User user) {
        User userToEdit=userRepository.findByUserName(user.getUserName());

        if (userToEdit == null) {
            return false;
        }

        if (user.getUserName().equals("")
                || user.getName().equals("")
                || user.getSurname().equals("")
                || user.getMail().equals("")
                ) {
            return false;
        }

        userToEdit.setName(user.getName());
        userToEdit.setSurname(user.getSurname());
        userToEdit.setMail(user.getMail());
        if (!user.getPassword().equals("")) {
            userToEdit.setPassword(user.getPassword());
        }
        userToEdit.setProgramRole(user.getProgramRole());

        userRepository.save(userToEdit);

        return true;

    }
}
