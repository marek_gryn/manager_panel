package managerpanel.service;

import managerpanel.model.Project;
import managerpanel.model.Task;
import managerpanel.model.User;
import managerpanel.repository.ProjectRepository;
import managerpanel.repository.TaskRepository;
import managerpanel.repository.UserRepository;
import managerpanel.utils.TaskToProjectAddingForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

@Service
public class TaskService {

    @Autowired
    TaskRepository taskRepository;

    @Autowired
    ProjectRepository projectRepository;

    @Autowired
    UserRepository userRepository;

    @Transactional
    public boolean addTaskToProject(TaskToProjectAddingForm taskToProjectAddingForm) {
        if (taskToProjectAddingForm.getEndDate() == null
                || taskToProjectAddingForm.getStartDate() == null
                || taskToProjectAddingForm.getPredictedTimeForTask()==null) {
            return false;
        }

        if (taskToProjectAddingForm.getName().equals("")
                || taskToProjectAddingForm.getDescription().equals("")
                || taskToProjectAddingForm.getPredictedTimeForTask().toString().equals("")
                || taskToProjectAddingForm.getPredictedTimeForTask() < 0) {
            return false;
        }

        Project project = projectRepository.findOne(taskToProjectAddingForm.getProjectId());
        if (project == null) {
            return false;
        }

        if (checkIfThereIsTaskWithGivenNameInProject(project, taskToProjectAddingForm.getName())) {
            return false;
        }

        if (taskToProjectAddingForm.getStartDate().before(project.getStartDate())
                || taskToProjectAddingForm.getEndDate().after(project.getEndDate())) {
            return false;
        }


        if (taskToProjectAddingForm.getEndDate().before(taskToProjectAddingForm.getStartDate())) {
            return false;
        }


        if (project.checkIfTaskWithGivenNameIsInProject(taskToProjectAddingForm.getName())) {
            return false;
        }

        Task task = new Task();

        task.setName(taskToProjectAddingForm.getName());
        task.setDescription(taskToProjectAddingForm.getDescription());
        task.setPredictedTimeForTask(taskToProjectAddingForm.getPredictedTimeForTask());
        task.setDescription(taskToProjectAddingForm.getDescription());
        task.setStartDate(taskToProjectAddingForm.getStartDate());
        task.setEndDate(taskToProjectAddingForm.getEndDate());

        task.setProject(project);
        if (taskToProjectAddingForm.getUserId() != null) {
            User user = userRepository.findOne(taskToProjectAddingForm.getUserId());
            task.setUser(user);
            user.getTaskList().add(task);
            project.getTaskList().add(task);
            taskRepository.save(task);
            userRepository.save(user);
        } else {
            project.getTaskList().add(task);
            taskRepository.save(task);
        }


        projectRepository.save(project);

        return true;
    }



    public Task findTask(Long id) {
        Task task=taskRepository.findOne(id);
        return task;
    }

    @Transactional
    public boolean updateTask(Task task) {
        if (task == null) {
            return false;
        }

        if (task.getCurrentSpentTime() == null) {
            return false;
        }

        if (task.getCurrentSpentTime() < 0) {
            return false;
        }

        Task taskToUpdate = taskRepository.findOne(task.getId());

        if (taskToUpdate == null) {
            return false;
        }

        taskToUpdate.setCurrentSpentTime(task.getCurrentSpentTime());

        if (task.getIsFinished()) {
            taskToUpdate.setPercetOfCompletion(100L);
        }
        else {
            taskToUpdate.setPercetOfCompletion(task.getPercetOfCompletion());
        }

        taskToUpdate.setIsFinished(task.getIsFinished());

        taskRepository.save(taskToUpdate);
        return true;
    }

    @Transactional
    public boolean editTask(TaskToProjectAddingForm taskFromForm) {
        if (taskFromForm == null) {
            return false;
        }

        if (taskFromForm.getName().equals("")) {
            return false;
        }

        if (taskFromForm.getEndDate() == null
                || taskFromForm.getStartDate() == null) {
            return false;
        }

        Task taskToEdit = taskRepository.findOne(taskFromForm.getTaskIdToEdit());

        if (taskToEdit == null) {
            return false;
        }

        if (!taskToEdit.getName().equals(taskFromForm.getName())) {
            if (checkIfThereIsTaskWithGivenNameInProject(taskToEdit.getProject(), taskFromForm.getName())){
                return false;
            }
        }

        taskToEdit.setName(taskFromForm.getName());

        taskToEdit.setDescription(taskFromForm.getDescription());

        if (taskFromForm.getPredictedTimeForTask() == null) {
            taskToEdit.setPredictedTimeForTask(0);
        }else if (taskFromForm.getPredictedTimeForTask() < 0) {
            return false;
        }else {
            taskToEdit.setPredictedTimeForTask(taskFromForm.getPredictedTimeForTask());
        }

        if (taskFromForm.getCurrentSpentTime() == null) {
            taskToEdit.setCurrentSpentTime(0);
        }else if (taskFromForm.getCurrentSpentTime() < 0) {
            return false;
        }else {
            taskToEdit.setCurrentSpentTime(taskFromForm.getCurrentSpentTime());
        }

        taskToEdit.setPercetOfCompletion(taskFromForm.getPercetOfCompletion());

        if (taskFromForm.getEndDate().before(taskFromForm.getStartDate())) {
            return false;
        }

        taskToEdit.setStartDate(taskFromForm.getStartDate());
        taskToEdit.setEndDate(taskFromForm.getEndDate());



        if (taskFromForm.getIsFinished()) {
            taskToEdit.setPercetOfCompletion(100L);
        }
        else {
            taskToEdit.setPercetOfCompletion(taskFromForm.getPercetOfCompletion());
        }

        User userFromForm = userRepository.findById(taskFromForm.getUserId());
        User userFromTask = taskToEdit.getUser();



        if (userFromTask == null) {
            taskToEdit.setUser(userFromForm);
            if (userFromForm != null) {
                userFromForm.getTaskList().add(taskToEdit);
                userRepository.save(userFromForm);
            }
        } else {
            if (userFromForm.getId() != userFromTask.getId()) {
                System.out.println(userFromTask.getTaskList().remove(taskToEdit));
                userFromForm.getTaskList().add(taskToEdit);
                taskToEdit.setUser(userFromForm);
                userRepository.save(userFromForm);
                userRepository.save(userFromTask);
            }
        }


        taskToEdit.setIsFinished(taskFromForm.getIsFinished());

        taskRepository.save(taskToEdit);
        return true;
    }

    private boolean checkIfThereIsTaskWithGivenNameInProject(Project project, String newTaskName) {
        Set<Task> tasks = project.getTaskList();
        for (Task t : tasks) {
            if (t.getName().equals(newTaskName)) {
                return true;
            }
        }
        return false;
    }
}
