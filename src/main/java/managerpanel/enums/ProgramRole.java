package managerpanel.enums;

import java.util.ArrayList;
import java.util.List;

public enum ProgramRole {
    ADMIN,
    MANAGER,
    USER;

    public static List<ProgramRole> getProgramRoles() {
        List<ProgramRole> programRoles = new ArrayList<>();
        programRoles.add(ProgramRole.USER);
        programRoles.add(ProgramRole.MANAGER);
        programRoles.add(ProgramRole.ADMIN);
        return programRoles;
    }
}
