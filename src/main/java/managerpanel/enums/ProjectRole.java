package managerpanel.enums;

import java.util.ArrayList;
import java.util.List;

public enum ProjectRole {
    PROJECT_MANAGER,
    USER;

    public static List<ProjectRole> getProgramRoles() {
        List<ProjectRole> projectRoles = new ArrayList<>();
        projectRoles.add(ProjectRole.USER);
        projectRoles.add(ProjectRole.PROJECT_MANAGER);
        return projectRoles;
    }
}
