package managerpanel.controller;


import managerpanel.model.Login;
import managerpanel.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@EnableWebMvc
@Controller
public class UserLoginController {


    @Autowired
    public UserService userService;

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView showLogin(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mav = new ModelAndView("login");
        mav.addObject("login", new Login());
        return mav;
    }

//    @RequestMapping(value = "/loggedUsers", method = RequestMethod.GET)
//    public ModelAndView showLogged(HttpServletRequest request, HttpServletResponse response) {
//
//        Cookie[] cookies = request.getCookies();
//        boolean isUserLogged = userService.returnOptionalUserIfLogged(cookies);
//        System.out.println("sprawdzenie w looged user czy widac zalogowanego: "+isUserLogged);
//
//        ModelAndView mav = new ModelAndView("loggedUsers");
//        mav.addObject("loggedUsersKeys", userService.getCurrentlyLoggedUsers().keySet());
//        mav.addObject("loggedUsers", userService.getCurrentlyLoggedUsers());
//        return mav;
//    }


    @RequestMapping(value = "/loginProcess", method = RequestMethod.POST)
    public ModelAndView loginProcess(HttpServletRequest request, HttpServletResponse response,
                        @ModelAttribute("login") Login login,
                        RedirectAttributes redirectAttributes) throws IOException {

        String loginControlCookieValue = String.valueOf(System.currentTimeMillis());

        boolean loginSuccessfull = userService.login(login, loginControlCookieValue);



        if (!loginSuccessfull) {

//            redirectAttributes.addAttribute("message","INVALID LOGIN PROCESS");
//            response.sendRedirect(request.getContextPath() + "/login");

            return new ModelAndView("login", "message", "INVALID LOGIN PROCESS");
        } else {
            Cookie cookie = new Cookie("loginControl", loginControlCookieValue);
            response.addCookie(cookie);

//            response.sendRedirect(request.getContextPath() + "/index.jsp");
            ModelAndView mav = new ModelAndView("index", "message", "Hello " + login.getUserName() + "!");
            return mav;
        }
    }

    @RequestMapping(value = "/logoutProcess", method = RequestMethod.GET)
    public ModelAndView addUser(HttpServletRequest request, HttpServletResponse response) {

        Cookie[] cookies = request.getCookies();
        for (Cookie c : cookies) {
            if (c.getName().equals("loginControl")) {
                c.setMaxAge(0);
                response.addCookie(c);
                userService.logout(c.getValue());
            }
        }
        ModelAndView mav = new ModelAndView("login", "message", "You have been logout");
        mav.addObject("login", new Login());
        return mav;

    }
}