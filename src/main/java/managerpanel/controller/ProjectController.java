package managerpanel.controller;


import managerpanel.enums.ProgramRole;
import managerpanel.enums.ProjectRole;
import managerpanel.model.Project;
import managerpanel.model.Task;
import managerpanel.model.User;
import managerpanel.service.ProjectService;
import managerpanel.service.UserService;
import managerpanel.utils.UserToProjectAddingForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Controller
public class ProjectController {

    @Autowired
    ProjectService projectService;

    @Autowired
    UserService userService;

    @InitBinder("project")
    protected void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        binder.registerCustomEditor(Date.class, new CustomDateEditor(
                dateFormat, true));
    }


    @RequestMapping(value = "/projects", method = RequestMethod.GET)
    public ModelAndView showProjects(HttpServletRequest request, HttpServletResponse response) {

        List<Project> projects = projectService.getAllProjects();

        ModelAndView mav = new ModelAndView("projects");
        mav.addObject("projects", projects);
        return mav;
    }

    @RequestMapping(value = "/projectDetails", method = RequestMethod.POST)
    public ModelAndView showProject(HttpServletRequest request, HttpServletResponse response,
                                    @RequestParam("projectId") Long id) {

        User user = (User) request.getAttribute("currentUser");
        Project project = projectService.getProject(id);

        ModelAndView mav = new ModelAndView("projectDetails");
        mav.addObject("project", project);
        mav.addObject("projectRole", returnProjectRoleOfGivenUser(project, user));
        return mav;
    }

    @RequestMapping(value = "/addProject", method = RequestMethod.GET)
    public ModelAndView showProjectAddForm(HttpServletRequest request, HttpServletResponse response) {

        ModelAndView mav = new ModelAndView("projectAddForm");
        mav.addObject("project", new Project());

        return mav;
    }


    @RequestMapping(value = "/addProjectProcess", method = RequestMethod.POST)
    public ModelAndView addProject(HttpServletRequest request, HttpServletResponse response,
                                   @ModelAttribute("project") Project project,
                                   BindingResult bindingResult) {

        boolean projectAddingSuccessful = projectService.addProject(project);

        if (!projectAddingSuccessful) {
            return new ModelAndView("projectAddForm", "message", "PROJECT ADDING FORM HAS NOT BEEN FILLED PROPERLY");
        } else {
            List<Project> projects = projectService.getAllProjects();

            ModelAndView mav = new ModelAndView("projects");
            mav.addObject("projects", projects);
            mav.addObject("message", "PROJECT " + project.getName() + " HAS BEEN SUCCESSFULLY ADDED");

            return mav;
        }
    }

    @RequestMapping(value = "/addUserToProjectForm", method = RequestMethod.POST)
    public ModelAndView addUserToProjectForm(HttpServletRequest request, HttpServletResponse response,
                                             @RequestParam("projectId") Long id) {

        List<User> users = userService.findAllUsers();

        List<ProjectRole> projectRoles = ProjectRole.getProgramRoles();

        ModelAndView mav = new ModelAndView("addUserToProjectForm");
        mav.addObject("userToProjectAddingForm", new UserToProjectAddingForm());
        mav.addObject("projectRoles", projectRoles);
        mav.addObject("users", users);
        mav.addObject("projectId", id);
        return mav;
    }


    @RequestMapping(value = "/addUserToProjectProcess", method = RequestMethod.POST)
    public ModelAndView addingUserToProjectProcess(HttpServletRequest request, HttpServletResponse response,
                                                   @ModelAttribute("addUserToProjectForm") UserToProjectAddingForm userToProjectAddingForm) {

        boolean userToProjectAddingSuccessful =
                projectService.addUserToProject(userToProjectAddingForm.getProjectId(),
                        userToProjectAddingForm.getUserId(),
                        userToProjectAddingForm.getProjectRole());

        if (!userToProjectAddingSuccessful) {
            return new ModelAndView("addUserToProjectForm", "message", "USER TO PROJECT ADDING FORM HAS NOT BEEN FILLED PROPERLY");
        } else {
            Project project = projectService.getProject(userToProjectAddingForm.getProjectId());
            User user = userService.findUser(userToProjectAddingForm.getUserId());
            List<Project> projects = projectService.getAllProjects();

            ModelAndView mav = new ModelAndView("projects");
            mav.addObject("message", "USER " + user.getName()
                    + " HAS BEEN SUCCESSFULLY ADDED TO " + project.getName() + " PROJECT");
            mav.addObject("projects", projects);
            return mav;
        }
    }

    @RequestMapping(value = "/removeUserFromProjectProcess", method = RequestMethod.POST)
    public ModelAndView removeUserFromProjectProcess(HttpServletRequest request, HttpServletResponse response,
                                                     @RequestParam("projectId") Long projectId,
                                                     @RequestParam("userId") Long userId) {

        User user = userService.findUser(userId);
        Project project = projectService.getProject(projectId);

        projectService.removeUserFromProject(user,project);

        List<Project> projects = projectService.getAllProjects();

        ModelAndView mav = new ModelAndView("projects");
        mav.addObject("message", "USER " + user.getName()
                + " HAS BEEN SUCCESSFULLY REMOVED FROM " + project.getName() + " PROJECT");
        mav.addObject("projects", projects);
        return mav;
    }

    private String returnProjectRoleOfGivenUser(Project project, User user) {
        Map<User, ProjectRole> userProjectRoleMap = project.getUsersInProject();

        for (Map.Entry<User, ProjectRole> entry : userProjectRoleMap.entrySet()) {
            if (entry.getKey().getId() == user.getId()) {
                return entry.getValue().toString();
            }
        }
        return "USER";
    }
}
