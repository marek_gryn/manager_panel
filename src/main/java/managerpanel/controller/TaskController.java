package managerpanel.controller;

import managerpanel.model.Project;
import managerpanel.model.Task;
import managerpanel.model.User;
import managerpanel.service.ProjectService;
import managerpanel.service.TaskService;
import managerpanel.utils.TaskToProjectAddingForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Controller
public class TaskController {

    @Autowired
    TaskService taskService;

    @Autowired
    ProjectService projectService;

    @InitBinder("taskToProjectAddingForm")
    protected void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        binder.registerCustomEditor(Date.class, new CustomDateEditor(
                dateFormat, true));
    }

    @RequestMapping(value = "/addTaskToProjectForm", method = RequestMethod.POST)
    public ModelAndView addTaskToProject(HttpServletRequest request, HttpServletResponse response,
                                         @RequestParam("projectId") Long id) {

        Set<User> users = projectService.getProject(id).getUsersInProject().keySet();

        ModelAndView mav = new ModelAndView("taskToProjectAddingForm");
        mav.addObject("taskToProjectAddingForm", new TaskToProjectAddingForm());
        mav.addObject("users", users);
        mav.addObject("projectId", id);
        return mav;
    }

    @RequestMapping(value = "/addTaskToProjectProcess", method = RequestMethod.POST)
    public ModelAndView addTaskToProjectProcess(HttpServletRequest request, HttpServletResponse response,
                                                @ModelAttribute("taskToProjectAddingForm") TaskToProjectAddingForm taskToProjectAddingForm,
                                                BindingResult bindingResult) throws IOException {

        boolean taskToProjectAddingSuccessful = taskService.addTaskToProject(taskToProjectAddingForm);

        if (!taskToProjectAddingSuccessful) {
            Set<User> users = projectService.getProject(taskToProjectAddingForm.getProjectId()).getUsersInProject().keySet();

            ModelAndView mav = new ModelAndView("taskToProjectAddingForm");
            mav.addObject("taskToProjectAddingForm", new TaskToProjectAddingForm());
            mav.addObject("users", users);
            mav.addObject("projectId", taskToProjectAddingForm.getProjectId());
            mav.addObject("message", "TASK TO PROJECT ADDING FORM HAS NOT BEEN FILLED PROPERLY");
            return mav;
        } else {
            Project project = projectService.getProject(taskToProjectAddingForm.getProjectId());


            List<Project> projects = projectService.getAllProjects();

            ModelAndView mav = new ModelAndView("projects");
            mav.addObject("message", "TASK " + taskToProjectAddingForm.getName()
                    + " HAS BEEN SUCCESSFULLY ADDED TO "+taskToProjectAddingForm.getName()+" PROJECT");
            mav.addObject("projects", projects);
            return mav;
        }

    }

    @RequestMapping(value = "/updateTaskForm", method = RequestMethod.POST)
    public ModelAndView updateTaskForm(HttpServletRequest request, HttpServletResponse response,
                                       @RequestParam("taskIdToUpdate") Long taskId) {

        Task task = taskService.findTask(taskId);

        ModelAndView mav = new ModelAndView("updateTaskForm");
        mav.addObject("taskToUpdate", task);
        return mav;
    }

    @RequestMapping(value = "/updateTaskProcess", method = RequestMethod.POST)
    public ModelAndView updateTaskProcess(HttpServletRequest request, HttpServletResponse response,
                                                @ModelAttribute("taskToUpdate") Task taskToUpdate,
                                                BindingResult bindingResult) {

        boolean taskUpdateSuccessful = taskService.updateTask(taskToUpdate);


        if (!taskUpdateSuccessful) {


            Task task = taskService.findTask(taskToUpdate.getId());

            ModelAndView mav = new ModelAndView("updateTaskForm");
            mav.addObject("taskToUpdate", task);
            mav.addObject("message", "TASK UPDATING FORM HAS NOT BEEN FILLED PROPERLY");
            return mav;
        } else {

            ModelAndView mav = new ModelAndView("index");
            mav.addObject("message", "TASK " + taskToUpdate.getName() + " HAS BEEN SUCCESSFULLY UPDATED");
            return mav;
        }
    }

    @RequestMapping(value = "/editTaskForm", method = RequestMethod.POST)
    public ModelAndView editTaskForm(HttpServletRequest request, HttpServletResponse response,
                                       @RequestParam("taskIdToEdit") Long taskId) {

        Task task = taskService.findTask(taskId);
        Set<User> users = projectService.getProject(task.getProject().getId()).getUsersInProject().keySet();

        ModelAndView mav = new ModelAndView("editTaskForm");
        mav.addObject("taskToEdit", task);
        mav.addObject("taskToProjectEditForm", new TaskToProjectAddingForm());
        mav.addObject("users", users);
        return mav;
    }

    @RequestMapping(value = "/editTaskProcess", method = RequestMethod.POST)
    public ModelAndView editTaskProcess(HttpServletRequest request, HttpServletResponse response,
                                          @ModelAttribute("taskToProjectEditForm") TaskToProjectAddingForm taskToProjectEditingForm,
                                          BindingResult bindingResult) {

        boolean taskUpdateSuccessful = taskService.editTask(taskToProjectEditingForm);


        if (!taskUpdateSuccessful) {


            Task task = taskService.findTask(taskToProjectEditingForm.getTaskIdToEdit());
            Set<User> users = projectService.getProject(task.getProject().getId()).getUsersInProject().keySet();

            ModelAndView mav = new ModelAndView("editTaskForm");
            mav.addObject("taskToProjectEditForm", new TaskToProjectAddingForm());
            mav.addObject("taskToEdit", task);
            mav.addObject("users", users);
            mav.addObject("message", "TASK EDITING FORM HAS NOT BEEN FILLED PROPERLY");
            return mav;
        } else {

            ModelAndView mav = new ModelAndView("index");
            mav.addObject("message", "TASK " + taskToProjectEditingForm.getName() + " HAS BEEN SUCCESSFULLY EDITED");
            return mav;
        }
    }
}

