package managerpanel.controller;


import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import managerpanel.enums.ProgramRole;
import managerpanel.model.User;

import managerpanel.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@Controller
public class UserRegistrationController {


    @Autowired
    public UserService userService;

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public ModelAndView showRegister(HttpServletRequest request, HttpServletResponse response) {

        List<ProgramRole> programRoles = ProgramRole.getProgramRoles();

        ModelAndView mav = new ModelAndView("register");
        mav.addObject("user", new User());
        mav.addObject("programRoles", programRoles);
        return mav;
    }


    @RequestMapping(value = "/registerProcess", method = RequestMethod.POST)
    public ModelAndView addUser(HttpServletRequest request, HttpServletResponse response,
                                @ModelAttribute("user") User user) {

        boolean registerSuccessfull = userService.register(user);

        if (!registerSuccessfull) {

            return new ModelAndView("register", "message", "USER WITH GIVEN USERNAME ALREADY IS DATABASE OR ALL FIELDS HAS NOT BEEN FILLED");
        } else {
            ModelAndView mav = new ModelAndView("index", "message", "USER " + user.getUserName() + " HAS BEEN ADDED TO DATABASE");

            return mav;
        }
    }

    @RequestMapping(value = "/registredUsers", method = RequestMethod.GET)
    public ModelAndView getRegistredUsers(HttpServletRequest request, HttpServletResponse response) {

        List<User> users = userService.findAllUsers();

        ModelAndView mav = new ModelAndView("registredUsers", "users", users);

        return mav;
    }

    @RequestMapping(value = "/deleteUser", method = RequestMethod.POST)
    public ModelAndView deleteUser(HttpServletRequest request, HttpServletResponse response,
                                   @RequestParam("userIdToDelete") Long id) {
        try {
            userService.deleteUser(id);
        } catch (DataIntegrityViolationException e) {
            List<User> users = userService.findAllUsers();

            ModelAndView mav = new ModelAndView("registredUsers", "users", users);
            mav.addObject("message", "THIS USER CAN NOT BE DELETED - USER IS IN PROJECTS OR/AND HAS A TASKS");
            return mav;
        }


        List<User> users = userService.findAllUsers();

        ModelAndView mav = new ModelAndView("registredUsers", "users", users);
        return mav;
    }

    @RequestMapping(value = "/editUser", method = RequestMethod.POST)
    public ModelAndView showEditUser(HttpServletRequest request, HttpServletResponse response,
                                     @RequestParam("userIdToEdit") Long id) {
        List<ProgramRole> programRoles = ProgramRole.getProgramRoles();
        User userToEdit=userService.findUser(id);

        ModelAndView mav = new ModelAndView("editUser");
        mav.addObject("userToEdit", userToEdit);
        mav.addObject("programRoles", programRoles);
        return mav;
    }

    @RequestMapping(value = "/editUserProcess", method = RequestMethod.POST)
    public ModelAndView editUser(HttpServletRequest request, HttpServletResponse response,
                                @ModelAttribute("user") User user) {

        boolean editSuccessfull = userService.editUser(user);

        if (!editSuccessfull) {
            List<ProgramRole> programRoles = ProgramRole.getProgramRoles();
            User userToEdit=userService.findUser(user.getId());
            ModelAndView mav=new ModelAndView("editUser", "message", "ALL FIELDS HAS NOT BEEN FILLED");
            mav.addObject("userToEdit", userToEdit);
            mav.addObject("programRoles", programRoles);
            return mav;
        } else {
            List<User> users = userService.findAllUsers();
            ModelAndView mav = new ModelAndView("registredUsers", "message", "USER " + user.getUserName() + " HAS BEEN EDITED");
            mav.addObject("users", users);
            return mav;
        }
    }
}