package managerpanel.model;

import managerpanel.enums.ProjectRole;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.*;

@Entity
public class Project {


    @Id
    @GeneratedValue
    private Long id;
    private String name;
    private String description;
    private String location;

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date startDate;


    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date endDate;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "users_projectRole")
    @MapKeyColumn(name = "<user>")
    @Enumerated(value = EnumType.STRING)
    private Map<User, ProjectRole> usersInProject;

    @OneToMany(fetch = FetchType.EAGER) //eager - zachłanny, będzie robił zapytania z joinem
    @JoinColumn(name = "PROJECT_ID")
    private Set<Task> taskList;

    public Project() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Map<User, ProjectRole> getUsersInProject() {
        return usersInProject;
    }

    public void setUsersInProject(Map<User, ProjectRole> usersInProject) {
        this.usersInProject = usersInProject;
    }

    public Set<Task> getTaskList() {
        return taskList;
    }

    public void setTaskList(Set<Task> taskList) {
        this.taskList = taskList;
    }

    public boolean checkIfTaskWithGivenNameIsInProject(String taskName) {
        for (Task t : taskList) {
            if (t.getName().equals(taskName)) {
                return true;
            }
        }
        return false;
    }
}
