package managerpanel.model;


import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Task {

    @Id
    @GeneratedValue
    private Long id;
    private String name;
    private String description;
    private Long percetOfCompletion;
    private Integer predictedTimeForTask;
    private Integer currentSpentTime;
    private Boolean isFinished;

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date startDate;

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date endDate;

    public Task() {
        this.percetOfCompletion=0L;
        this.currentSpentTime=0;
        this.isFinished=false;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "PROJECT_ID")
    private Project project;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "USER_ID")
    private User user;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getPercetOfCompletion() {
        return percetOfCompletion;
    }

    public void setPercetOfCompletion(Long percetOfCompletion) {
        this.percetOfCompletion = percetOfCompletion;
    }

    public Integer getPredictedTimeForTask() {
        return predictedTimeForTask;
    }

    public void setPredictedTimeForTask(Integer predictedTimeForTask) {
        this.predictedTimeForTask = predictedTimeForTask;
    }

    public Integer getCurrentSpentTime() {
        return currentSpentTime;
    }

    public void setCurrentSpentTime(Integer currentSpentTime) {
        this.currentSpentTime = currentSpentTime;
    }

    public Boolean getIsFinished() {
        return isFinished;
    }

    public void setIsFinished(Boolean finished) {
        isFinished = finished;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }
}
