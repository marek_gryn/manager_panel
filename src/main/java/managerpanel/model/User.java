package managerpanel.model;

import managerpanel.enums.ProgramRole;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.FetchProfile;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
public class User implements Serializable {
    @Id
    @GeneratedValue
    private Long id;
    private String userName;
    private String name;
    private String surname;
    private String mail;
    private String password;
    @Enumerated(value = EnumType.STRING)
    private ProgramRole programRole;

    @Fetch(FetchMode.JOIN)
    @OneToMany(fetch = FetchType.EAGER) //eager - zachłanny, będzie robił zapytania z joinem
    @JoinColumn(name = "USER_ID")
    private List<Task> taskList;


    public User() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public ProgramRole getProgramRole() {
        return programRole;
    }

    public void setProgramRole(ProgramRole programRole) {
        this.programRole = programRole;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public List<Task> getTaskList() {
        return taskList;
    }

    public void setTaskList(List<Task> taskList) {
        this.taskList = taskList;
    }

    @Override
    public String toString() {
        return name+" "+surname;
    }
}
