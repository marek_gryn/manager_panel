package managerpanel.filter;

import managerpanel.enums.ProgramRole;
import managerpanel.model.Project;
import managerpanel.model.User;
import managerpanel.service.UserService;

import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;


public class TestFilter implements Filter {


    private final UserService userService;

    public TestFilter(UserService userService) {
        super();
        this.userService = userService;
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        Cookie[] cookies = request.getCookies();

        Optional<User> userOptional = userService.returnOptionalUserIfLogged(cookies);
        User currentUser=null;

        String path = request.getServletPath();

        if (userOptional.isPresent()){
            currentUser=userOptional.get();
            request.setAttribute("currentUser", currentUser);
        }

        if (!userOptional.isPresent() && !path.endsWith("login") && !path.endsWith("loginProcess") && !path.endsWith("logoutProcess")) {
            response.sendRedirect(request.getContextPath() + "/login");
        } else {
            String programRole="";
            if (currentUser != null) {
                if (currentUser.getProgramRole() == ProgramRole.ADMIN) {
                    programRole = "ADMIN";
                } else if (currentUser.getProgramRole() == ProgramRole.MANAGER) {
                    programRole = "MANAGER";
                } else {
                    programRole = "USER";
                }

                request.setAttribute("programRole", programRole);
            }

            if (path.endsWith("register") && !programRole.equals("ADMIN")) {
                response.sendRedirect(request.getContextPath() + "/registredUsers");
            }

            filterChain.doFilter(request, response);
        }

//        filterChain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }
}
